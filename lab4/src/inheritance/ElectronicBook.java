// Kausar Mussa
// Student ID: 1738212
package inheritance;

public class ElectronicBook extends Book {
private double numberBytes;

public ElectronicBook ( String title, String author, double numberBytes) {
	super(title, author); //call the Book constructor and specify title and author
	this.numberBytes= numberBytes;
}

//overriding toString from Book class
@Override
public String toString() {
	String fromBase = super.toString();
	return fromBase + " Number of Bytes: " + numberBytes;
}
}
