// Kausar Mussa
// Student ID: 1738212
package inheritance;

public class Book {
	protected String title;
	private String author;
	
	//constructor
	public Book (String title, String author) {
		this.title=title;
		this.author= author;	
	}
	//getters
	public String getTitle() {
		return this.title;
	}
	public String getAuthor() {
		return this.author;
	}
	//overriding the toString() method
	@Override
	public String toString() {
		return " Title of the book: " + title + " written by: " + author;
	}
	
}
