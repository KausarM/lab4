// Kausar Mussa
// Student ID: 1738212
package inheritance;

public class BookStore {

	public static void main(String[] args) {
		// main method
		Book [] library = new Book [5];
		library[0]= new Book ("Star Wars","George Lucas");//Book
		library[1]=	new ElectronicBook ("The Zoo","Elephant King", 50);//ElectronicBook
		library[2]= new Book ("Curious George 2","Margaret Rey");//Book
		library[3]= new ElectronicBook ("Curious Monkey","Margaret ", 65);//ElectronicBook
		library[4]= new ElectronicBook ("Curious George","Margaret Rey", 40);//ElectronicBook
		
		//loop to print values of the Book []
		for( int i=0; i < library.length; i++) {
			System.out.println(library[i]);
		}
	}

}
