// Kausar Mussa
// Student ID: 1738212
package geometry;

public class Circle implements Shape {
private double radius;

public Circle (double radius) {
	this.radius = radius;
}
public double getRadius() {
	return this.radius;
}

public double getArea() {
	return Math.PI * radius * radius;
}
public double getPerimeter() {
	return 2* Math.PI * radius;
}

/*test to make sure Circle class works
public static void main(String[] args) {
	Circle test = new Circle (3);
	System.out.println(test.getArea());
	System.out.println(test.getPerimeter());
} */

}
