// Kausar Mussa
// Student ID: 1738212
package geometry;

public interface Shape {

	double getArea();
	double getPerimeter();
}
