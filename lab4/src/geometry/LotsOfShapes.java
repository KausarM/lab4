// Kausar Mussa
// Student ID: 1738212
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		// // main method
		Shape [] alot = new Shape [5];
		alot[0] = new Rectangle (6,7);
		alot[1] = new Rectangle (5,4);
		alot[2] = new Circle (6);
		alot[3] = new Circle (8);
		alot[4] = new Square (9);
		
		//loop to print every shape's area and perimeter
		for( int i= 0; i< alot.length; i++) {
			System.out.println("Area of shape is " + alot[i].getArea());
			System.out.println("Perimeter of shape is " + alot[i].getPerimeter());
		}

	}

}

