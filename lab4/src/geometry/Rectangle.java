// Kausar Mussa
// Student ID: 1738212
package geometry;

public class Rectangle implements Shape {
private double length;
private double width;

public Rectangle (double length, double width) {
	this.length=length;
	this.width=width;
	}
public double getLength() {
	return this.length;
}
	
public double getWidth() {
	return this.width;
}
public double getArea() {
	return length * width;
}
public double getPerimeter() {
	return length+length+width+width;
}

/*test to make sure Rectangle class works
public static void main(String[] args) {
	Rectangle test = new Rectangle (3,4);
	System.out.println(test.getArea());
	System.out.println(test.getPerimeter());
} */
}
