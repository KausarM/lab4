// Kausar Mussa
// Student ID: 1738212
package geometry;

public class Square extends Rectangle {

	public Square (double sideLength) {
		super(sideLength,sideLength);	
	}
	/*test to make sure Square class works
	public static void main(String[] args) {
		Rectangle testAgain = new Square (5);
		System.out.println(testAgain.getArea());
		System.out.println(testAgain.getPerimeter());
	} */
}
